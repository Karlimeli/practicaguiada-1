package facci.karlasanchez.am1practicaguiada1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("AM1 Practica Guiada1","Sánchez Sancán Karla Melissa");
    }
    public void add(View v)
    {
        TextView result=(TextView)findViewById(R.id.result);
        EditText et1=(EditText)findViewById(R.id.editText1);


        //get value from edit text box and convert into double
        double a=Double.parseDouble(String.valueOf(et1.getText()));
        RadioButton cb=(RadioButton)findViewById(R.id.cb);
        RadioButton fb=(RadioButton)findViewById(R.id.fb);


        //check which radio button is checked
        if(cb.isChecked())
        {

            //display conversion
            result.setText(f2c(a)+" Degree C");
            //cb.setChecked(false);

        }
        else
        {
            result.setText(c2f(a)+" Degree F");
            //fb.setChecked(false);

        }
    }
    //Celcius to Fahrenhiet method
    private double c2f(double c)
    {
        return (c*9)/5+32;
    }
    //Fahrenhiet to Celcius method
    private double f2c(double f)
    {
        return (f-32)*5/9;
    }

}
